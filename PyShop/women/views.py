from django.shortcuts import render
from django.http import HttpResponse
from .models import Women


# Create your views here.

def women1 (request):
    women = Women.objects.all()
    return render(request, 'women.html',{'women': women})

