from django.contrib import admin
from .models import Women


# Register your models here.

class WomenAdmin(admin.ModelAdmin):
    list_display = ('name','price','stock')

admin.site.register(Women,WomenAdmin)

