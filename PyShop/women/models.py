from django.db import models

# Create your models here.

class Women(models.Model):
    name = models.CharField(max_length=255)
    price = models.FloatField()
    stock = models.IntegerField(max_length=255)
    image_url = models.CharField(max_length=2042)