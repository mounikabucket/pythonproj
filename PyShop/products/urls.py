from django.urls import path,include
from . import views



urlpatterns = [
    path('', views.index),
    path('women/',include('women.urls')),
    path('men/', include('men.urls')),
    path('kids/', include('kids.urls'))

]